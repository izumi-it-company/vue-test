# Vue test

## Create `<Autocomplete />` component.

#### Component details

1. Props:
    - items - `string[]` - data for search.
2. Slots:
    - default - HTMLInputElement. (By default `<Autocomplete />` renders
    simple input).
3. Create delay on every user input events to prevent unnecessary API call
(preferably use RxJs).

4. Write several unit test in the `tests/unit` for `<Autocomplete />`.
(preferably use Jest).

#### Requirements:
 - Use eslint with prettier.
 - Use TypeScript (preferably use tsx syntax).
 - Use `Vue.extend` or `Vue.component` to create component.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```
